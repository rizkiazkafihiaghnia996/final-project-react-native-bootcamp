import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Navigation from './App/navigations'

const App = () => {
    return (
        <Navigation />
    )
}

export default App

const styles = StyleSheet.create({})