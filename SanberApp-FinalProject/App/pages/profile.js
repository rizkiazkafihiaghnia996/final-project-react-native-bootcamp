import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from 'react-native'
import Button from '../component/Button/button'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Icon2 from 'react-native-vector-icons/FontAwesome'
import Icon3 from 'react-native-vector-icons/Ionicons'
import { useFonts, RopaSans_400Regular, RopaSans_400Regular_Italic } from '@expo-google-fonts/ropa-sans'
import AppLoading from 'expo-app-loading'
import navigation from '../navigations'


const Profile = ({navigation}) => {
    let [fontsLoaded, error] = useFonts({
        RopaSansRegular: RopaSans_400Regular,
        RopaSansItalic: RopaSans_400Regular_Italic
    })

    if (!fontsLoaded) {
        return <AppLoading />
    }

    return (
        <View style={styles.container}>
            <ScrollView>
            <View style={styles.aboutMe}>
                <Icon style={styles.profilePicture} name="account-circle" size={180}/>
                <Text style={styles.namaSaya}>Rizki Azka Fihi Aghnia</Text>
            </View>
            <View style={styles.portfolioBar}>
                <Text style={styles.title}>PORTFOLIO</Text>
                <View style={styles.titleLine} />
                <View style={styles.portfolioContent}>
                    <TouchableOpacity>
                        <Icon3 style={styles.steamIcon} name='logo-steam' size={40} />
                        <Text style={styles.steamTitle}>BlackCypher</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon style={styles.steamIcon} name='origin' size={40} />
                        <Text style={styles.steamTitle}>BlackCypher99</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.contactMeBar}>
                <Text style={styles.title}>CONTACT ME</Text>
                <View style={styles.titleLine} />
                    <TouchableOpacity>
                        <View style={styles.facebook}>
                            <Icon2 style={styles.contactIcon} name='facebook-square' size={40} />
                            <Text style={styles.contactTitle}>Rizki Azka Fihi A</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.instagram}>
                            <Icon2 style={styles.contactIcon} name='instagram' size={40} />
                            <Text style={styles.contactTitle}>@rizki_a.f.a</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.twitter}>
                            <Icon2 style={styles.contactIcon} name='twitter' size={40} />
                            <Text style={styles.contactTitle}>@azka_rizki</Text>
                        </View>
                    </TouchableOpacity>
            </View>
            <View style={styles.signOutButton}>
                <Button title='Sign Out' onPress={() => navigation.navigate('Login')} />
            </View>
            </ScrollView>
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'yellow',
        flexDirection: 'column'
    },
    aboutMe: {
        alignItems: 'center',
        paddingTop: 15
    },
    profilePicture: {
        marginTop: -10,
        color: '#3DD67A',
        elevation: 3
    },
    namaSaya: {
        fontFamily: "Roboto",
        fontSize: 18,
        fontWeight: 'bold',
        color: 'black',
    },
    portfolioBar: {
        height: 110,
        backgroundColor: '#383737',
        borderRadius: 10,
        marginTop: 15,
        marginLeft: 9,
        marginRight: 9,
        flexDirection: 'column'
    },
    title: {
        fontFamily: 'RopaSansRegular',
        fontSize: 17,
        color: '#3DD67A',
        paddingLeft: 10,
        paddingTop: 3
    },
    titleLine: {
        backgroundColor: '#3DD67A',
        height: 1.5,
        marginTop: 5,
        marginLeft: 10,
        marginRight: 10
    },
    portfolioContent: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingTop: 5
    },
    steamIcon: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 20,
        color: 'white'
    },
    steamTitle: {
        color: '#3DD67A',
        fontWeight: 'bold'
    },
    contactMeBar: {
        height: 190,
        backgroundColor: '#383737',
        borderRadius: 10,
        marginTop: 15,
        marginLeft: 9,
        marginRight: 9,
        flexDirection: 'column'
    },
    facebook: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    instagram: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    twitter: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    contactIcon: {
        paddingLeft: 100,
        paddingTop: 10,
        color: 'white'
    },
    contactTitle: {
        paddingLeft: 20,
        paddingTop: 10,
        color: '#3DD67A',
        fontWeight: 'bold'
    },
    signOutButton: {
        height: 35,
        width: 100,
        paddingHorizontal: 5,
        backgroundColor: 'red',
        alignItems: 'center',
        borderRadius: 10,
        marginBottom: 70,
        marginTop: 15,
        marginLeft: 130,
        elevation: 3
    }
})