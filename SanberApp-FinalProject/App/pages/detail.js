import React, {Component} from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity, ScrollView } from 'react-native'
import { useFonts, RopaSans_400Regular, RopaSans_400Regular_Italic } from '@expo-google-fonts/ropa-sans'
import navigation from '../navigations'



export default class App extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {thumbnail, title, genre, rating, description} = this.props.route.params
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Image source={{uri: thumbnail}} style={{ width: 360, height: 200 }} />
                    <View style={styles.detailContentBar}>
                        <Text style={styles.gameTitle}>{title}</Text>
                        <Text style={styles.gameGenre}>Genre: {genre}</Text>
                        <Text style={styles.gameRating}>Rating: {rating}</Text>
                        <Text style={styles.gameDesc}>{description}</Text>
                        <TouchableOpacity>
                            <View style={styles.playButton}>
                                <Text style={styles.playTitle}>Play</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'yellow'
    },
    detailContentBar: {
        height: 280,
        backgroundColor: '#383737',
        borderRadius: 10,
        marginTop: 15,
        marginLeft: 9,
        marginRight: 9
    },
    gameTitle: {
        fontFamily: 'RopaSansRegular',
        fontSize: 20,
        color: 'white',
        paddingLeft: 4,
        paddingTop: 5
    },
    gameGenre: {
        paddingLeft: 5,
        fontSize: 12,
        color: 'white'
    },
    gameRating: {
        paddingLeft: 5,
        fontSize: 12,
        color: 'white'  
    },
    gameDesc: {
        color: '#3DD67A',
        paddingLeft: 5,
        paddingRight: 5
    },
    playButton: {
        height: 35,
        width: 100,
        backgroundColor: 'orange',
        paddingHorizontal: 5,
        marginTop: 10,
        marginLeft: 120,
        borderRadius: 10
    },
    playTitle: {
        paddingTop: 7,
        fontFamily: 'RopaSansRegular',
        fontSize: 17,
        paddingLeft: 30
    }
})