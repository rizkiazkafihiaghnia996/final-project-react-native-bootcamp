import LoginRegister from './login-register'
import Login from './login'
import Register from './register'
import Home from './home'
import Detail from './detail'
import Profile from './profile'

export { LoginRegister, Login, Register, Home, Detail, Profile }