import React, { Component } from 'react'
import { StyleSheet, Text, View, FlatList, Image, TouchableOpacity, ActivityIndicator } from 'react-native'
import { useFonts, RopaSans_400Regular, RopaSans_400Regular_Italic } from '@expo-google-fonts/ropa-sans'
import navigation from '../navigations'

export default class Home extends Component {
    constructor() {
        super()
        this.state = {
            dataSource: [],
            isLoading: true
        }
    }

    renderItem = ({ item }) => {
        return (
            <View style={styles.content}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Detail', { thumbnail: item.snippet.thumbnail, title: item.snippet.title, genre: item.snippet.genre, rating: item.snippet.rating, description: item.snippet.description })}>
                    <Image source={{ uri: item.snippet.thumbnail }} style={styles.thumbnailSize} />
                    <View style={styles.contentBar}>
                        <Text style={styles.gameTitle}>{item.snippet.title}</Text>
                        <Text style={styles.gamePrice}>Price: {item.snippet.price}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    componentDidMount() {
        const url = 'http://www.json-generator.com/api/json/get/crdDQkqwia?indent=2'
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    dataSource: responseJson.results,
                    isLoading: false
                })
            })
            .catch((error) => {
                console.log(error)
            })
    }

    render() {
        return (
            this.state.isLoading
            ?
            <View style={styles.loading}>
                <ActivityIndicator size='large' color='#3DD67A' animating />
            </View>
            :
            <View style={styles.container}>
                <FlatList
                    data={this.state.dataSource}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index}
                    ItemSeparatorComponent={() => <View style={{height: 0.5, width: '100%', backgroundColor: "black"}} />}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'yellow'
    },
    content: {
        paddingTop: 8,
    },
    contentBar: {
        backgroundColor: 'black'
    },
    gameTitle: {
        fontFamily: 'RopaSansRegular',
        fontWeight: '600',
        fontSize: 20,
        color: 'white',
        paddingLeft: 4
    },
    thumbnailSize: {
        width: 360,
        height: 200
    },
    gamePrice: {
        paddingLeft: 4,
        fontSize: 12,
        color: '#3DD67A'
    },
    loading: {
        flex: 1,
        backgroundColor: 'yellow',
        justifyContent: 'center',
        alignItems: 'center'
    },
})