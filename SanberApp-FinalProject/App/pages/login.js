import React from 'react'
import { StyleSheet, Text, TextInput, View, Image } from 'react-native'
import Button from '../component/Button/button'
import AppLoading from 'expo-app-loading'
import { useFonts, RopaSans_400Regular, RopaSans_400Regular_Italic } from '@expo-google-fonts/ropa-sans'

import navigation from '../navigations'


const Login = ({navigation}) => {
    let [fontsLoaded, error] = useFonts({
        RopaSansRegular: RopaSans_400Regular,
        RopaSansItalic: RopaSans_400Regular_Italic
    })
    if (!fontsLoaded) {
        return <AppLoading />
    }

    return (
        <View style={styles.container}>
            <View style={styles.appLogo}>
                <Image source={require('../component/Images/NexusLogo.png')} style={{width: 350, height: 130}}/>
                <Text style={styles.loginText}>LOGIN</Text>
            </View>
            <View style={styles.credentials}>
                <TextInput style={styles.inputCredentials} placeholder=' Username/E-mail' placeholderTextColor='white' />
                <TextInput style={styles.inputCredentials} placeholder=' Password' placeholderTextColor='white'/>
            </View>
            <View style={styles.loginButton}>
                <Button title='Login' onPress={() => navigation.navigate('tabStack')} />
            </View>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'yellow'
    },
    appLogo: {
        paddingTop: 80,
        alignItems: 'center'
    },
    loginText: {
        fontFamily: 'RopaSansRegular',
        fontSize: 24,
        color: 'black',
        paddingTop: 20,
    },
    credentials: {
        paddingTop: 25,
        paddingLeft: 32,
        flexDirection: 'column',
        fontSize: 16,
        fontFamily: 'RopaSansRegular',
        color: '#003366'
    },
    inputCredentials: {
        height: 40,
        width: 294,
        borderColor: 'black',
        borderWidth: 1,
        marginBottom: 15,
        backgroundColor: '#383737',
        borderRadius: 8
    },
    loginButton: {
        height: 35,
        width: 100,
        paddingHorizontal: 5,
        backgroundColor: '#3DD67A',
        alignItems: 'center',
        borderRadius: 10,
        marginTop: 10,
        marginLeft: 130,
        elevation: 3
    }
})
