import React, {useState} from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import Button from '../component/Button/button'
import { useFonts, RopaSans_400Regular, RopaSans_400Regular_Italic } from '@expo-google-fonts/ropa-sans'
import AppLoading from 'expo-app-loading'

import navigation from '../navigations'

const LoginRegister = ({navigation}) => {
    let [fontsLoaded, error] = useFonts({
        RopaSansRegular: RopaSans_400Regular,
        RopaSansItalic: RopaSans_400Regular_Italic
    })

    if (!fontsLoaded) {
        return <AppLoading />
    }

    return (
        <View style={styles.container}>
            <View style={styles.appLogo}>
                <Image source={require('../component/Images/NexusLogo.png')} style={{width: 350, height: 130}}/>
            </View>
            <View style={styles.loginregisterBar}>
                <View style={styles.registerButton}>
                    <Button title='Register' onPress={() => navigation.navigate('Register')} />
                </View>
                <Text style={styles.loginTitle}>Already have an account?</Text>
                <View style={styles.loginButton}>
                    <Button title='Login' onPress={() => navigation.navigate('Login')} />
                </View>
            </View>
        </View>
    )
}

export default LoginRegister

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'yellow'
    },
    appLogo: {
        paddingTop: 150
    },
    loginregisterBar: {
        height: 250,
        backgroundColor: '#383737',
        borderRadius: 10,
        marginTop: 25,
        marginLeft: 40,
        marginRight: 40,
        flexDirection: 'column'
    },
    registerButton: {
        height: 35,
        width: 100,
        paddingHorizontal: 5,
        backgroundColor: '#3DD67A',
        alignItems: 'center',
        borderRadius: 10,
        marginTop: 70,
        marginLeft: 90,
        elevation: 3
    },
    loginTitle: {
        paddingTop: 30,
        paddingLeft: 75,
        color: 'white',
        fontFamily: 'RopaSansRegular'
    },
    loginButton: {
        height: 35,
        width: 100,
        paddingHorizontal: 5,
        backgroundColor: '#3DD67A',
        alignItems: 'center',
        borderRadius: 10,
        marginTop: 10,
        marginLeft: 90,
        elevation: 3
    }
})