import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import {NavigationContainer} from '@react-navigation/native'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'

import { 
    LoginRegister, 
    Login, 
    Register, 
    Home, 
    Profile, 
    Detail 
} from '../pages'
import TabBar from '../component/TabBar'

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

const navigation = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name='Nexus' component={LoginRegister} 
                    options={
                        {
                            headerShown: false
                        }
                    }
                />
                <Stack.Screen name='Register' component={Register} 
                    options={
                        {
                            title: '',
                            headerStyle: {
                                backgroundColor: 'yellow'
                            },
                            headerTransparent: true
                        }
                    }
                />
                <Stack.Screen name='Login' component={Login} 
                    options={
                        {
                            title: '',
                            headerStyle: {
                                backgroundColor: 'yellow'
                            },
                            headerTransparent: true
                        }
                    }
                />
                <Stack.Screen name='tabStack' component={tabStack} 
                    options={
                        {
                            title: '',
                            headerStyle: {
                                backgroundColor: 'yellow'
                            },
                            headerTransparent: false
                        }
                    }
                />
                <Stack.Screen name='Detail' component={Detail} 
                    options={
                        {
                            title: '',
                            headerStyle: {
                                backgroundColor: 'yellow'
                            },
                            headerTransparent: false
                        }
                    }
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const tabStack = () => {
    return(
        <Tab.Navigator tabBar={(props) => <TabBar {...props} /> }>
            <Tab.Screen name='Home' component={Home} initialParams={{ icon: 'home' }} />
            <Tab.Screen name='Profile' component={Profile} initialParams={{ icon: 'user' }} />
        </Tab.Navigator>
    )
}


export default navigation

