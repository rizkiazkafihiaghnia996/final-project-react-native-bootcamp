import React from 'react'
import { Text, TouchableOpacity, StyleSheet } from 'react-native'
import { useFonts, RopaSans_400Regular, RopaSans_400Regular_Italic } from '@expo-google-fonts/ropa-sans'

const Button = ({ title, onPress }) => {
    let [fontsLoaded, error] = useFonts({
        RopaSansRegular: RopaSans_400Regular,
        RopaSansItalic: RopaSans_400Regular_Italic
    })

    return (
        <TouchableOpacity onPress={onPress}>
            <Text style={styles.buttonText}>{title}</Text>
        </TouchableOpacity>
    )
}

export default Button

const styles = StyleSheet.create({
    buttonText: {
        paddingTop: 7,
        fontFamily: 'RopaSansRegular',
        fontSize: 17
    }
})
