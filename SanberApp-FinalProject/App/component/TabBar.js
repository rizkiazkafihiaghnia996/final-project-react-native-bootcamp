import React, {useState} from 'react'
import { View, StyleSheet, Dimensions } from 'react-native'
import Tab from './Tab'

const {width} = Dimensions.get('screen')

const TabBar = ({state, navigation}) => {
    const [selected, setSelected] = useState('Home')
    const {routes} = state
    const renderColor = currentTab => (currentTab === selected ? '#3DD67A' : 'yellow')
    
    const handlePress = (activeTab, index) => {
        if(state.index !== index) {
            setSelected(activeTab)
            navigation.navigate(activeTab)
        }
    }

    return (
        <View style={styles.wrapper}>
            <View style={styles.container}>
                {routes.map((route, index) => (
                  <Tab 
                    tab={route} 
                    icon={route.params.icon} 
                    onPress={ () => handlePress(route.name, index)} 
                    color={renderColor(route.name)} 
                    key={route.key} 
                />
                ))}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        position: 'absolute',
        bottom: 10,
        width,
        alignItems: 'center',
        justifyContent: 'center'
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'black',
        width: 250,
        borderRadius: 20,
        elevation: 3
    }
})

export default TabBar